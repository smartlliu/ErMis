package org.apache.jsp.view;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class forgot_005fpassword_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html class='no-js' lang='en'>\n");
      out.write("  <head>\n");
      out.write("    <meta charset='utf-8'>\n");
      out.write("    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>\n");
      out.write("    <title>Forgot password</title>\n");
      out.write("    <meta content='lab2023' name='author'>\n");
      out.write("    <meta content='' name='description'>\n");
      out.write("    <meta content='' name='keywords'>\n");
      out.write("    <link href=\"assets/stylesheets/application-a07755f5.css\" rel=\"stylesheet\" type=\"text/css\" /><link href=\"//netdna.bootstrapcdn.com/font-awesome/3.2.0/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("    <link href=\"assets/images/favicon.ico\" rel=\"icon\" type=\"image/ico\" />\n");
      out.write("    \n");
      out.write("  </head>\n");
      out.write("  <body class='login'>\n");
      out.write("    <div class='wrapper'>\n");
      out.write("      <div class='row'>\n");
      out.write("        <div class='col-lg-12'>\n");
      out.write("          <form>\n");
      out.write("            <fieldset>\n");
      out.write("              <legend>Reset your password</legend>\n");
      out.write("            </fieldset>\n");
      out.write("            <div class='form-group'>\n");
      out.write("              <label class='control-label'>Email address</label>\n");
      out.write("              <input class='form-control' placeholder='Enter your email address' type='text'>\n");
      out.write("            </div>\n");
      out.write("            <a class=\"btn btn-default\" href=\"/\">Send</a>\n");
      out.write("            <a href=\"/ErMis\">Go back</a>\n");
      out.write("          </form>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("    <!-- Footer -->\n");
      out.write("    <!-- Javascripts -->\n");
      out.write("    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\" type=\"text/javascript\"></script><script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js\" type=\"text/javascript\"></script><script src=\"//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js\" type=\"text/javascript\"></script><script src=\"assets/javascripts/application-dd9177ab.js\" type=\"text/javascript\"></script>\n");
      out.write("    <!-- Google Analytics -->\n");
      out.write("    <script>\n");
      out.write("      var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];\n");
      out.write("      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n");
      out.write("      g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';\n");
      out.write("      s.parentNode.insertBefore(g,s)}(document,'script'));\n");
      out.write("    </script>\n");
      out.write("  </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
