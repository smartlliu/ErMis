package org.apache.jsp.view;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class dashboard_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html class='no-js' lang='en'>\n");
      out.write("  <head>\n");
      out.write("    <meta charset='utf-8'>\n");
      out.write("    <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>\n");
      out.write("    <title>Dashboard</title>\n");
      out.write("    <meta content='lab2023' name='author'>\n");
      out.write("    <meta content='' name='description'>\n");
      out.write("    <meta content='' name='keywords'>\n");
      out.write("    <link href=\"assets/stylesheets/application-a07755f5.css\" rel=\"stylesheet\" type=\"text/css\" /><link href=\"//netdna.bootstrapcdn.com/font-awesome/3.2.0/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("    <link href=\"assets/images/favicon.ico\" rel=\"icon\" type=\"image/ico\" />\n");
      out.write("    \n");
      out.write("  </head>\n");
      out.write("  <body class='main page'>\n");
      out.write("    <!-- Navbar -->\n");
      out.write("    <div class='navbar navbar-default' id='navbar'>\n");
      out.write("      <a class='navbar-brand' href='#'>\n");
      out.write("        <i class='icon-beer'></i>\n");
      out.write("      ErMis\n");
      out.write("      </a>\n");
      out.write("      <ul class='nav navbar-nav pull-right'>\n");
      out.write("        <li class='dropdown'>\n");
      out.write("          <a class='dropdown-toggle' data-toggle='dropdown' href='#'>\n");
      out.write("            <i class='icon-envelope'></i>\n");
      out.write("            Messages\n");
      out.write("            <span class='badge'>5</span>\n");
      out.write("            <b class='caret'></b>\n");
      out.write("          </a>\n");
      out.write("          <ul class='dropdown-menu'>\n");
      out.write("            <li>\n");
      out.write("              <a href='#'>New message</a>\n");
      out.write("            </li>\n");
      out.write("            <li>\n");
      out.write("              <a href='#'>Inbox</a>\n");
      out.write("            </li>\n");
      out.write("            <li>\n");
      out.write("              <a href='#'>Out box</a>\n");
      out.write("            </li>\n");
      out.write("            <li>\n");
      out.write("              <a href='#'>Trash</a>\n");
      out.write("            </li>\n");
      out.write("          </ul>\n");
      out.write("        </li>\n");
      out.write("        <li>\n");
      out.write("          <a href='#'>\n");
      out.write("            <i class='icon-cog'></i>\n");
      out.write("            Settings\n");
      out.write("          </a>\n");
      out.write("        </li>\n");
      out.write("        <li class='dropdown user'>\n");
      out.write("          <a class='dropdown-toggle' data-toggle='dropdown' href='#'>\n");
      out.write("            <i class='icon-user'></i>\n");
      out.write("            <strong>John DOE</strong>\n");
      out.write("            <img class=\"img-rounded\" src=\"http://placehold.it/20x20/ccc/777\" />\n");
      out.write("            <b class='caret'></b>\n");
      out.write("          </a>\n");
      out.write("          <ul class='dropdown-menu'>\n");
      out.write("            <li>\n");
      out.write("              <a href='#'>Edit Profile</a>\n");
      out.write("            </li>\n");
      out.write("            <li class='divider'></li>\n");
      out.write("            <li>\n");
      out.write("              <a href=\"/\">Sign out</a>\n");
      out.write("            </li>\n");
      out.write("          </ul>\n");
      out.write("        </li>\n");
      out.write("      </ul>\n");
      out.write("    </div>\n");
      out.write("    <div id='wrapper'>\n");
      out.write("      <!-- Sidebar -->\n");
      out.write("      <section id='sidebar'>\n");
      out.write("        <i class='icon-align-justify icon-large' id='toggle'></i>\n");
      out.write("        <ul id='dock'>\n");
      out.write("          <li class='active launcher'>\n");
      out.write("            <i class='icon-dashboard'></i>\n");
      out.write("            <a href=\"dashboard.html\">Dashboard</a>\n");
      out.write("          </li>\n");
      out.write("          <li class='launcher'>\n");
      out.write("            <i class='icon-file-text-alt'></i>\n");
      out.write("            <a href=\"forms.html\">Forms</a>\n");
      out.write("          </li>\n");
      out.write("          <li class='launcher'>\n");
      out.write("            <i class='icon-table'></i>\n");
      out.write("            <a href=\"tables.html\">Tables</a>\n");
      out.write("          </li>\n");
      out.write("          <li class='launcher dropdown hover'>\n");
      out.write("            <i class='icon-flag'></i>\n");
      out.write("            <a href='#'>Reports</a>\n");
      out.write("            <ul class='dropdown-menu'>\n");
      out.write("              <li class='dropdown-header'>Launcher description</li>\n");
      out.write("              <li>\n");
      out.write("                <a href='#'>Action</a>\n");
      out.write("              </li>\n");
      out.write("              <li>\n");
      out.write("                <a href='#'>Another action</a>\n");
      out.write("              </li>\n");
      out.write("              <li>\n");
      out.write("                <a href='#'>Something else here</a>\n");
      out.write("              </li>\n");
      out.write("            </ul>\n");
      out.write("          </li>\n");
      out.write("          <li class='launcher'>\n");
      out.write("            <i class='icon-bookmark'></i>\n");
      out.write("            <a href='#'>Bookmarks</a>\n");
      out.write("          </li>\n");
      out.write("          <li class='launcher'>\n");
      out.write("            <i class='icon-cloud'></i>\n");
      out.write("            <a href='#'>Backup</a>\n");
      out.write("          </li>\n");
      out.write("          <li class='launcher'>\n");
      out.write("            <i class='icon-bug'></i>\n");
      out.write("            <a href='#'>Feedback</a>\n");
      out.write("          </li>\n");
      out.write("        </ul>\n");
      out.write("        <div data-toggle='tooltip' id='beaker' title='Made by lab2023'></div>\n");
      out.write("      </section>\n");
      out.write("      <!-- Tools -->\n");
      out.write("      <section id='tools'>\n");
      out.write("        <ul class='breadcrumb' id='breadcrumb'>\n");
      out.write("          <li class='title'>Dashboard</li>\n");
      out.write("          <li><a href=\"#\">Lorem</a></li>\n");
      out.write("          <li class='active'><a href=\"#\">ipsum</a></li>\n");
      out.write("        </ul>\n");
      out.write("        <div id='toolbar'>\n");
      out.write("          <div class='btn-group'>\n");
      out.write("            <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Building'>\n");
      out.write("              <i class='icon-building'></i>\n");
      out.write("            </a>\n");
      out.write("            <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Laptop'>\n");
      out.write("              <i class='icon-laptop'></i>\n");
      out.write("            </a>\n");
      out.write("            <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Calendar'>\n");
      out.write("              <i class='icon-calendar'></i>\n");
      out.write("              <span class='badge'>3</span>\n");
      out.write("            </a>\n");
      out.write("            <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Lemon'>\n");
      out.write("              <i class='icon-lemon'></i>\n");
      out.write("            </a>\n");
      out.write("          </div>\n");
      out.write("          <div class='label label-danger'>\n");
      out.write("            Danger\n");
      out.write("          </div>\n");
      out.write("          <div class='label label-info'>\n");
      out.write("            Info\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("      </section>\n");
      out.write("      <!-- Content -->\n");
      out.write("      <div id='content'>\n");
      out.write("        <div class='panel panel-default'>\n");
      out.write("          <div class='panel-heading'>\n");
      out.write("            <i class='icon-beer icon-large'></i>\n");
      out.write("            Hierapolis Rocks!\n");
      out.write("            <div class='panel-tools'>\n");
      out.write("              <div class='btn-group'>\n");
      out.write("                <a class='btn' href='#'>\n");
      out.write("                  <i class='icon-refresh'></i>\n");
      out.write("                  Refresh statics\n");
      out.write("                </a>\n");
      out.write("                <a class='btn' data-toggle='toolbar-tooltip' href='#' title='Toggle'>\n");
      out.write("                  <i class='icon-chevron-down'></i>\n");
      out.write("                </a>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("          <div class='panel-body'>\n");
      out.write("            <div class='page-header'>\n");
      out.write("              <h4>System usage</h4>\n");
      out.write("            </div>\n");
      out.write("            <div class='progress'>\n");
      out.write("              <div class='progress-bar progress-bar-success' style='width: 35%'></div>\n");
      out.write("              <div class='progress-bar progress-bar-warning' style='width: 20%'></div>\n");
      out.write("              <div class='progress-bar progress-bar-danger' style='width: 10%'></div>\n");
      out.write("            </div>\n");
      out.write("            <div class='page-header'>\n");
      out.write("              <h4>User statics</h4>\n");
      out.write("            </div>\n");
      out.write("            <div class='row text-center'>\n");
      out.write("              <div class='col-md-3'>\n");
      out.write("                <input class='knob second' data-bgcolor='#d4ecfd' data-fgcolor='#30a1ec' data-height='140' data-inputcolor='#333' data-thickness='.3' data-width='140' type='text' value='50'>\n");
      out.write("              </div>\n");
      out.write("              <div class='col-md-3'>\n");
      out.write("                <input class='knob second' data-bgcolor='#c4e9aa' data-fgcolor='#8ac368' data-height='140' data-inputcolor='#333' data-thickness='.3' data-width='140' type='text' value='75'>\n");
      out.write("              </div>\n");
      out.write("              <div class='col-md-3'>\n");
      out.write("                <input class='knob second' data-bgcolor='#cef3f5' data-fgcolor='#5ba0a3' data-height='140' data-inputcolor='#333' data-thickness='.3' data-width='140' type='text' value='35'>\n");
      out.write("              </div>\n");
      out.write("              <div class='col-md-3'>\n");
      out.write("                <input class='knob second' data-bgcolor='#f8d2e0' data-fgcolor='#b85e80' data-height='140' data-inputcolor='#333' data-thickness='.3' data-width='140' type='text' value='85'>\n");
      out.write("              </div>\n");
      out.write("            </div>\n");
      out.write("          </div>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("    <!-- Footer -->\n");
      out.write("    <!-- Javascripts -->\n");
      out.write("    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js\" type=\"text/javascript\"></script><script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js\" type=\"text/javascript\"></script><script src=\"//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js\" type=\"text/javascript\"></script><script src=\"assets/javascripts/application-dd9177ab.js\" type=\"text/javascript\"></script>\n");
      out.write("    <!-- Google Analytics -->\n");
      out.write("    <script>\n");
      out.write("      var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];\n");
      out.write("      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];\n");
      out.write("      g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';\n");
      out.write("      s.parentNode.insertBefore(g,s)}(document,'script'));\n");
      out.write("    </script>\n");
      out.write("  </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
