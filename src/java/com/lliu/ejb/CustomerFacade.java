/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lliu.ejb;

import com.lliu.entity.Customer;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author lliu
 */
@Stateless
public class CustomerFacade extends AbstractFacade<Customer> {
    @PersistenceContext(unitName = "ErMisPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomerFacade() {
        super(Customer.class);
    }
    public  Customer findWithEmail(String email)
    {
        Query query = em.createQuery("SELECT c FROM Customer c WHERE c.email =:ID");
        query.setParameter("ID",email );
        List<Customer> customers = null;
        Customer customer = null;
        customers = query.getResultList();
        if(customers == null)
        {
           
        }
        else
        {
            if(customers.isEmpty())
            {
                 customer = null;
            }else
            {
                customer = customers.get(0);
            }
           
        }
        
       return customer;
    }
}
