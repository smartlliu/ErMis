/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lliu.bean;

import com.lliu.ejb.BillFacade;
import com.lliu.ejb.CustomerFacade;
import com.lliu.entity.Admin;
import com.lliu.entity.Bill;
import com.lliu.entity.Customer;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author lliu
 */
@ManagedBean
@SessionScoped
public class CustomerBean {
    @EJB 
    private CustomerFacade customerFacade;
    private Customer customer;
    @EJB
    private BillFacade billFacade;
    private List<Bill> bills;

    public List<Bill> getBills() {
        return billFacade.findAll();
    }

    public Customer getCustomer() {
        return customer;
    }
    
    
    public void createCustomer()
    {
        customerFacade.create(customer);
    }
    
}
