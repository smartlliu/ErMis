/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lliu.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author lliu
 */
@Entity
@Table(name="customer")
public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="c_id")
    private Long id;
    
        private String email;

    /**
     * Get the value of email
     *
     * @return the value of email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Set the value of email
     *
     * @param email new value of email
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    
    private String c_name;

    /**
     * Get the value of c_name
     *
     * @return the value of c_name
     */
    public String getC_name() {
        return c_name;
    }

    /**
     * Set the value of c_name
     *
     * @param c_name new value of c_name
     */
    public void setC_name(String c_name) {
        this.c_name = c_name;
    }
    
    private String password;

    /**
     * Get the value of password
     *
     * @return the value of password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the value of password
     *
     * @param password new value of password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    private String c_imageurl;

    /**
     * Get the value of c_imageurl
     *
     * @return the value of c_imageurl
     */
    public String getC_imageurl() {
        return c_imageurl;
    }

    /**
     * Set the value of c_imageurl
     *
     * @param c_imageurl new value of c_imageurl
     */
    public void setC_imageurl(String c_imageurl) {
        this.c_imageurl = c_imageurl;
    }
    
    private long c_taxid;

    /**
     * Get the value of c_taxid
     *
     * @return the value of c_taxid
     */
    public long getC_taxid() {
        return c_taxid;
    }

    /**
     * Set the value of c_taxid
     *
     * @param c_taxid new value of c_taxid
     */
    public void setC_taxid(long c_taxid) {
        this.c_taxid = c_taxid;
    }

    private long c_acountid;

    /**
     * Get the value of c_acountid
     *
     * @return the value of c_acountid
     */
    public long getC_acountid() {
        return c_acountid;
    }

    /**
     * Set the value of c_acountid
     *
     * @param c_acountid new value of c_acountid
     */
    public void setC_acountid(long c_acountid) {
        this.c_acountid = c_acountid;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    private String c_address;

    /**
     * Get the value of c_address
     *
     * @return the value of c_address
     */
    public String getC_address() {
        return c_address;
    }

    /**
     * Set the value of c_address
     *
     * @param c_address new value of c_address
     */
    public void setC_address(String c_address) {
        this.c_address = c_address;
    }

    
    
    @ManyToOne
    private Admin admin;

    /**
     * Get the value of admin
     *
     * @return the value of admin
     */
    public Admin getAdmin() {
        return admin;
    }
    
    
    /**
     * Set the value of admin
     *
     * @param admin new value of admin
     */
    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

        private String phone;

    /**
     * Get the value of phone
     *
     * @return the value of phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set the value of phone
     *
     * @param phone new value of phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lliu.entity.Customer[ id=" + id + " ]";
    }
    
}
