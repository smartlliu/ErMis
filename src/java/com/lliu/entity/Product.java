/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lliu.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lliu
 */
@Entity
@Table(name="product")
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="p_id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    
    
        private String p_name;

    /**
     * Get the value of p_name
     *
     * @return the value of p_name
     */
    public String getP_name() {
        return p_name;
    }

    /**
     * Set the value of p_name
     *
     * @param p_name new value of p_name
     */
    public void setP_name(String p_name) {
        this.p_name = p_name;
    }
    
        private int p_number;

    /**
     * Get the value of p_number
     *
     * @return the value of p_number
     */
    public int getP_number() {
        return p_number;
    }

    /**
     * Set the value of p_number
     *
     * @param p_number new value of p_number
     */
    public void setP_number(int p_number) {
        this.p_number = p_number;
    }
    
    private float p_price;

    /**
     * Get the value of p_price
     *
     * @return the value of p_price
     */
    public float getP_price() {
        return p_price;
    }

    /**
     * Set the value of p_price
     *
     * @param p_price new value of p_price
     */
    public void setP_price(float p_price) {
        this.p_price = p_price;
    }
      
    @ManyToOne
    private Bill bill;

    /**
     * Get the value of bill
     *
     * @return the value of bill
     */
    public Bill getBill() {
        return bill;
    }

    /**
     * Set the value of bill
     *
     * @param bill new value of bill
     */
    public void setBill(Bill bill) {
        this.bill = bill;
    }

    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lliu.entity.Product[ id=" + id + " ]";
    }
    
}
