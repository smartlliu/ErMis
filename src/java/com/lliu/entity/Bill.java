/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lliu.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lliu
 */
@Entity
@Table(name="bill")
public class Bill implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="b_id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @ManyToOne
    private Customer customerid;

    /**
     * Get the value of customerid
     *
     * @return the value of customerid
     */
    public Customer getCustomerid() {
        return customerid;
    }

    /**
     * Set the value of customerid
     *
     * @param customerid new value of customerid
     */
    public void setCustomerid(Customer customerid) {
        this.customerid = customerid;
    }
        
    @ManyToOne
    private Admin adminid;

    /**
     * Get the value of adminid
     *
     * @return the value of adminid
     */
    public Admin getAdminid() {
        return adminid;
    }

    /**
     * Set the value of adminid
     *
     * @param adminid new value of adminid
     */
    public void setAdminid(Admin adminid) {
        this.adminid = adminid;
    }

        private int status;

    /**
     * Get the value of status
     *
     * @return the value of status
     */
    public int getStatus() {
        return status;
    }

    /**
     * Set the value of status
     *
     * @param status new value of status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    
    @OneToMany
    private List<Product> products;

    /**
     * Get the value of products
     *
     * @return the value of products
     */
    public List<Product> getProducts() {
        return products;
    }

    /**
     * Set the value of products
     *
     * @param products new value of products
     */
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bill)) {
            return false;
        }
        Bill other = (Bill) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lliu.entity.Bill[ id=" + id + " ]";
    }
    
}
